//-- ***** BEGIN LICENCE BLOCK *****
// - Copyright (c) 2005-2016 CoralTree Systems Ltd.    
// - All Rights Reserved.                            
// - ***** END LICENCE BLOCK *****  

//------------------------------------------------------------ 
//--  File:         app.js               
//--  Description:  This tiny node app test connecting to a data queue remotely                  
//--  Author:       Kevin Turner                                      
//--  Date:         Dec 2016                              
//------------------------------------------------------------ 
// 
// Modification log
// ================
//  Inits  Date     Modification           
//  =====  ====     ============
//
// ===================================================================

 
var timeoutMsg = "Timeout!";
var retryInterval = 100;  // wait 0.1 second to retry to get result in sync mode.
var conf 	= require("./lib/config");
var dq 		= require("./lib/idataq"); 
var xt 		= require("./lib/itoolkit");
 
// Augment toolkit to enable keyed data queues
augmentToolkit();

// Wait for stuff to arrive on a data queue also
retryTimes = Math.round(xt.timeout / retryInterval); 

conn = this.conn = new xt.iConn(conf.database, conf.user, conf.password, conf.options);
dtaq = new dq.iDataQueue(conn); 
 
// Kick the listening process off
console.log("Waiting for data for key "+conf.key+" of length "+conf.key.length);
dtaq.receiveFromKeyedDataQueue(conf.dtaq, conf.dtaqlib, conf.bufflen, conf.wait, conf.oper, conf.key.length, conf.key, cb);


function cb(str) {
	console.log(str)
	if (str.indexOf("<errnoxml>")>0) {
		process.exit(1);	
	}
	else {
		// Wait some more
		dtaq.receiveFromKeyedDataQueue(conf.dtaq, conf.dtaqlib, conf.bufflen, conf.wait, conf.oper, conf.key.length, conf.key, cb);
	}
}
	


/**
 * Augment toolkit to enable keyed data queues
 */	
function augmentToolkit(){
	//Add a new function to write to keyed data queues
	dq.iDataQueue.prototype.sendToKeyedDataQueue = function(name, lib, data, key) {
		var pgm = new xt.iPgm("QSNDDTAQ", {"lib":"QSYS"});
		pgm.addParam(name, "10A");
		pgm.addParam(lib == ""?"*CURLIB":lib, "10A");
		pgm.addParam(data.length, "5p0");
		pgm.addParam(data, data.length + "A");
		pgm.addParam(key.length, "3p0");
		pgm.addParam(key, key.length + "A");	

		this.conn.add(pgm.toXML());
		var rtValue;  // The returned value.
		var stop = 0;  // A flag indicating whether the process is finished.
		var retry = 0;  // How many times we have retried.
		function toJson(str) {  // Convert the XML output into JSON
			var output = xt.xmlToJson(str);
			if(output && output.length>0 && output[0].success)
				rtValue = true;
			else
				rtValue = str;
			stop = 1;
		}
		function waitForResult() {
			retry++;
			if(stop == 0)
				setTimeout('waitForResult()', retryInterval);  // Check whether the result is retrieved
			else if(retry >= retryTimes)
				return timeoutMsg;
			else
				return rtValue;
		}
		this.conn.run(toJson);  // Post the input XML and get the response.
		return waitForResult();  // Run the user defined call back function against the returned value.
	}


	//Add a new function to read keyed data queues
	dq.iDataQueue.prototype.receiveFromKeyedDataQueue = function(name, lib, length, wait, keyOp, keyLen, key, cb) {
		var pgm = new xt.iPgm("QRCVDTAQ", {"lib":"QSYS"});
		pgm.addParam(name, "10A");
		pgm.addParam(lib == ""?"*CURLIB":lib, "10A");
		pgm.addParam(length, "5p0");
		pgm.addParam("", length + 1 + "A");
		pgm.addParam(wait, "5p0");
		pgm.addParam(keyOp, "2A");
		pgm.addParam(keyLen, "3p0");
		pgm.addParam(key, key.length + "A");
		pgm.addParam(0, "3p0");
		pgm.addParam("", "1A");

		

		this.conn.add(pgm.toXML());
		var async = cb && xt.getClass(cb) == "Function"; //If there is a callback function param, then it is in asynchronous mode.
		var rtValue;  // The returned value.
		var stop = 0;  // A flag indicating whether the process is finished.
		var retry = 0;  // How many times we have retried.
		function toJson(str) {  // Convert the XML output into JSON
			var output = xt.xmlToJson(str);
			if(output && output.length>0 && output[0].success)
				rtValue = output[0].data[3].value;
			else
				rtValue = str;
			if(async)	// If it is in asynchronous mode.
				cb(rtValue);  // Run the call back function against the returned value.
			stop = 1;
		}
		function waitForResult() {
			retry++;
			if(stop == 0)
				setTimeout('waitForResult()', retryInterval);  // Check whether the result is retrieved
			else if(retry >= retryTimes)
				return timeoutMsg;
			else
				return rtValue;
		}
		this.conn.run(toJson);  // Post the input XML and get the response.
		if(!async)  // If it is in synchronized mode.
			return waitForResult();  // Run the user defined call back function against the returned value.
	}
}
 
