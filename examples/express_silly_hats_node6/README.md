# node.js tutorial
This tutorial is a simple nodejs express application with jade interface.

The focus of this project is using node with IBM i provided itoolkit REST interface.

The original project was for bluemix. This is a port to IBM i node 6.

##YIPS silly hats demo
* [YIPS silly hats](http://yips.idevcloud.com/silly/)


# The project
This is a traditional nodejs express/jade project. The project simulates adding GUI images in hat ordering. 
Also, enabled 'crude' textarea social media to chat the hat. The interface was intentionally simple (ugly),
to avoid reader getting lost in GUI details and focus on REST accessing IBM i data with itoolkit.

Note: While express/jade is most common application,
i recommend json api style interface like project bears, flight400. 

## Technical
Download available: https://bitbucket.org/litmis/nodejs/downloads

## IBM i library
* [YIPS hats.zip (hats.savf)](http://yips.idevcloud.com/wiki/index.php/NodeJs/NodeJs) - download library HATS RPG and DB2


# node

```
bash-4.3$ export PATH=/QOpenSys/QIBM/ProdData/OPS/Node6/bin:/QOpenSys/usr/bin
bash-4.3$ export LIBPATH=/QOpenSys/QIBM/ProdData/OPS/Node6/lib:/QOpenSys/usr/lib
bash-4.3$ node --version
v6.9.1
bash-4.3$ pwd
/home/node6/MyIBMiNodeJs
bash-4.3$ npm install

bash-4.3$ cat package.json 
{
  "name": "node-api",
  "main": "app.js",
  "dependencies": {
    "express": "~4.0.0",
    "body-parser": "~1.0.1",
    "jade": "~1.1.4"
  }
}

$ npm install
```

Traditionally, start node application by hand (node app.js).
This application was deployed using mama. Please refer to mama project for configuration.

## mama fastcgi (Apache integration).
* [YIPS Apache mama](https://bitbucket.org/litmis/mama)


