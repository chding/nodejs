/*jshint node:true*/

// app.js
// This file contains the server side JavaScript code for your application.
// This sample application uses express as web application framework (http://expressjs.com/),
// and jade as template engine (http://jade-lang.com/).

var express = require('express');

// setup middleware
var app = express();
var bodyParser = require('body-parser');
app.use(express.static(__dirname + '/public')); //setup static public directory
app.set('view engine', 'jade');
app.set('views', __dirname + '/views'); //optional since express defaults to CWD/views
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var port = process.env.PORT || 47720;        // set our port

// xmlservice
var XmlServiceProvider = require('./xmlserviceprovider').XmlServiceProvider;
var xmlserviceProvider= new XmlServiceProvider();

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
  next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working
router.get('/', function(req, res) {
  xmlserviceProvider.HatsCat(
    function(result) {
      res.render('index', { title: 'select hat group', items: result } );
    }
    );
});

router.get('/cat', function(req, res) {
  var key = req.query.key;
  xmlserviceProvider.HatsPgmCat(
    function(result) {
      res.render('cat', { title: 'select your hats', items: result } );
    }
    , key);
});


router.get('/big', function(req, res) {
  var key = req.query.key;
  var chat = null;
  xmlserviceProvider.HatsDetail(
    function(result) {
      res.render('big', { title: 'hat information', item: result } );
    }
    , key, chat);
});

router.post('/big', function(req,res) {
  var key = req.body.prod;
  var chat = req.body.chat;
  xmlserviceProvider.HatsDetail(
    function(result) {
      res.render('big', { title: 'hat information', item: result } );
    }
    , key, chat);
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/silly', router);

// START THE SERVER
// =============================================================================
app.listen(port);

