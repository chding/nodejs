//
// Show how to connect to DB2 for i from Node.js
//

var http = require('http')
var db = require('/QOpenSys/QIBM/ProdData/Node/os400/db2i/lib/db2')

var server = http.createServer(function (req, res) {
  db.init()
  db.conn("*LOCAL")
  db.exec("SELECT LSTNAM, STATE FROM QIWS.QCUSTCDT", function(rs) {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.end(JSON.stringify(rs))
  })
  db.close()
})

server.listen(8002)
var host = server.address().address
var port = server.address().port
console.log('Example app listening at http://%s:%s', host, port)