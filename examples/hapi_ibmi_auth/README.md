# HapiJs IBM i Authorization App
An app that shows how to authenticate an IBM i profile and password, and then create a session (cookie) to determine whether the user has been authenticated.

## Install
```
$ git clone ssh://git@bitbucket.org/litmis/hapi_ibmi_auth.git
$ cd hapi_ibmi_auth
$ npm install
```

## Run
**Note:** Running `env.sh` will set the environment; like the correct Node version.
```
$ . env.sh
$ npm start
```
Direct your browser at http://ibmi:port
