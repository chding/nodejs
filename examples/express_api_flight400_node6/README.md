#flight 400

Welcome to flight 400 project. A simple nodejs example of accessing IBM i flight400 RPG code.

The focus of this project is node with IBM i itoolkit.

The project is a multiple tier architecture for maximum flexibility. There are really two interfaces to Flight 400.

* First, traditional JQuery GUI make REST API calls to IBM i nodejs server through Apache (see conf).
* Second, programmable REST/JSON API interface (see API tab on application GUI). 

I suggest dual interface is best practice way to design any application you wish to export to web.
The programmable REST/JSON APIs allows extreme flexibility in designing custom interfaces to your valuable RPG logic.

The project directories:

* view - JQuery UI
* lib - LPP OPS node6 itoolkit
* flight400 - interface to RPG programs
* server.js - REST/JSON service based on express

##YIPS flight400 demo
* [YIPS flight400](http://yips.idevcloud.com/flight400/) (click on API tab for REST details)
* [YIPS flight400](http://yips.idevcloud.com/flight400/api)

# Technical
This project is pure REST. 
Everything is REST, JQuery GUI is using REST api (see view), node server.js using itoolkit REST to XMLSERVICE, etc. 

This type of 'pure REST' application is common in 'rich client' configurations. 
JQuery GUI view browser makes many 'async' REST requests (json response) to IBM i. 
To date, performance of pure REST application seems adequate.   

##node

```
bash-4.3$ export PATH=/QOpenSys/QIBM/ProdData/OPS/Node6/bin:/QOpenSys/usr/bin                                                                         
bash-4.3$ export LIBPATH=/QOpenSys/QIBM/ProdData/OPS/Node6/lib:/QOpenSys/usr/lib

bash-4.3$ node --version
v6.9.1

$ cat package.json 
{
    "name": "node-api",
    "main": "server.js",
    "dependencies": {
        "express": "~4.0.0",
        "body-parser": "~1.0.1"
    }
}

$ npm install
```

Traditionally, start node application by hand (node server.js).
This application was deployed using mama. Please refer to mama project for configuration.

## mama fastcgi (Apache integration).
* [YIPS Apache mama](https://bitbucket.org/litmis/mama)


##JQuery 
```
$ ls -1 view
flight400.css
flight400.js
index.html
```
* [JQuery UI help](http://api.jqueryui.com/category/widgets/)

## RPG
See README_RPG.md for flight 400 RPG download and RPG fix information (required fix).

